package com.example.demo.StudentDetails;

public class StudentDetail {
	private int id;
	private String name;
	private String address;
	private String qualification;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public StudentDetail(int id, String name, String address, String qualification) {
		
		this.id = id;
		this.name = name;
		this.address = address;
		this.qualification = qualification;
	}
	public StudentDetail() {
		
		// TODO Auto-generated constructor stub
	}
	

}
