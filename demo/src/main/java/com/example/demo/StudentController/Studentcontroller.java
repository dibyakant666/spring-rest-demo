package com.example.demo.StudentController;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.StudentDetails.StudentDetail;
import com.example.demo.StudentService.StudentService;


@RestController
public class Studentcontroller {
	@Autowired
	private StudentService service;
	
    @GetMapping(value="/getallstudentdetails"  )
	public List <StudentDetail> getDetails(){
    	return service.allstudentsdetails();
    }
    
   @GetMapping(value="/detailsbyid/{id}")
    public StudentDetail getStudent(@PathVariable("id") int id) {
    	return service.getStudent(id);
    }
   
    @PostMapping("/addstudents")
   	public void addDetails(@RequestBody StudentDetail StudentDetails){
       	 service.addDetails(StudentDetails);
    }
}
