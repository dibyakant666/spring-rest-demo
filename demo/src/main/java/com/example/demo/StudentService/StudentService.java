package com.example.demo.StudentService;


import java.util.*;

import org.springframework.stereotype.Service;

import com.example.demo.StudentDetails.StudentDetail;
@Service
public class StudentService {
	private  List<StudentDetail> Studentdetails = new ArrayList<>(Arrays.asList(new StudentDetail(101, "Shan", "Pune", "B.E."),
			new StudentDetail(102, "Dibyakant", "Bhubaneshwar", "B.Arch"),new StudentDetail(103, "Ajinkya", "Nagpur", "M.Tech"),
			new StudentDetail(104, "Tejas", "Mumbai", "M.B.A.")
			));
    public List<StudentDetail> allstudentsdetails(){
    	return Studentdetails;
    }
	public void addDetails(StudentDetail studentDetails2) {
		// TODO Auto-generated method stub
		Studentdetails.add(studentDetails2);
	}

	public StudentDetail getStudent(int id) {
		// TODO Auto-generated method stub
		return Studentdetails.stream().filter(sid ->sid.getId()==id).findFirst().get(); 
	}
}